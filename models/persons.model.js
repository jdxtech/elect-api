'use strict';
module.exports = function(sequelize, Sequelize) {
  var Persons = sequelize.define('persons_list', {
    id: { 
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: { 
      type: Sequelize.STRING,
      allowNull: false
    },
    photo: {
      type: Sequelize.STRING,
      allowNull: true
    },
    related_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    related_type: {
      type: Sequelize.STRING,
      allowNull: true
    },
    age: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    mobile: {
      type: Sequelize.STRING(512),
      allowNull: true
    },
    email: {
      type: Sequelize.STRING,
      allowNull: true
    },
    caste: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    sub_caste: {
       type: Sequelize.INTEGER,
       defaultValue: 0
      },
    education: {
        type: Sequelize.STRING,
        allowNull: true
    },
    qualification: {
        type: Sequelize.STRING,
        allowNull: true
    },
    occupation: {
        type: Sequelize.STRING,
        allowNull: true
    },
    occupation_type: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    voterid: {
        type: Sequelize.STRING,
        allowNull: true
    },
    door_no: {
        type: Sequelize.STRING,
        allowNull: true
    },
    street: {
        type: Sequelize.STRING,
        allowNull: false
    },
    pincode: {
        type: Sequelize.BIGINT,
        defaultValue: 0
    },
    area_id: {
        type: Sequelize.BIGINT,
        defaultValue: 0
    },
    
  });

  /*User.associate = function(models) {
  };*/  
	
  return Persons;
};