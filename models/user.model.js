'use strict';
module.exports = function (sequelize, Sequelize) {
    const User = sequelize.define('user', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
        {
            timestamps: false,
            defaultScope: {
                attributes: { exclude: ['password'] },
            }
        });

    return User;
};