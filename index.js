var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');
const config = require('config');

var indexRouter = require('./routes/index');
var personsRouter = require('./routes/persons');
const usersRouters = require('./routes/users');
const authRouters = require('./routes/auth');

var cors = require("cors");

var app = express();
app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', personsRouter);
app.use('/api/users', usersRouters);
app.use('/api/auth', authRouters);

// if (!config.get('jwtPrivateKey')) {
//   console.log('FATAL ERROR: jwtPrivateKey is not defined.');
//   process.exit(1);
// }

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;




// const express = require('express');

// const product = require('./routes/product.route'); // Imports routes for the products
// const app = express();

// app.use(bodyParser.json());

// const mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost:27017/productstutorial', {useNewUrlParser: true})
// .then(() => console.log('connecting to database successful'))
// .catch(err => console.error('could not connect to mongo DB', err))



let port = 9000;
app.listen(port, () => {
  console.log('Server is up and running on port numner ' + port);
});

