const db = require('../config/db.config.js');
const _ = require('lodash');
const User = db.user;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.createUser = async function (req, res) {
    try {
        let user = await User.findOne({ where: { email: req.body.email } });
        if (user) return res.status(400).send('User already registered.');

        const salt = await bcrypt.genSalt(10);
        req.body.password = await bcrypt.hash(req.body.password, salt);

        let newUser = await User.create(req.body, ['name', 'email', 'password']);
        const token = jwt.sign({ id: newUser.id }, 'jwtPrivateKey');
        res.header('x-auth-token', token).send(_.pick(newUser, ['id', 'name', 'email']));
    }
    catch (error) {
        res.send(error.message);
    }
}

exports.getCurrentUser = async function (req, res) {
    try {
        const user = await User.findByPk(req.user.id);
        res.send(user);
    }
    catch (error) {
        res.send(error.message);
    }
}

