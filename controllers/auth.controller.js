const db = require('../config/db.config.js');
const _ = require('lodash');
const User = db.user;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('config');

exports.login = async function (req, res) {
    try {
        let user = await User.findOne({ where: { email: req.body.email } });
        if (!user) return res.status(400).send('Invalid email or password.');

        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword)
            return res.status(400).send('Invalid email or password.');

        // const token = jwt.sign({ id: user.id }, config.get('elect_jwtPrivateKey'));
        const token = jwt.sign({ id: user.id }, 'jwtPrivateKey');

        res.send(token);
    }
    catch (error) {
        res.send(error.message);
    }
}

