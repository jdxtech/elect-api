const db = require('../config/db.config.js');
const Persons = db.persons;
 
// Post a Persons
exports.create = (req, res) => {  
  // Save to MySQL database
  Persons.create({  
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    age: req.body.age
  }).then(customer => {    
    // Send created customer to client
    res.send(customer);
  });
};
 
// FETCH all Personss
exports.findAll = (req, res) => {
  Persons.findAll().then(customers => {
    // Send all customers to Client
    res.send(customers);
  });
};
 
// Find a Persons by Id
exports.findById = (req, res) => {  
  Persons.findById(req.params.customerId).then(customer => {
    res.send(customer);
  })
};
 
// Update a Persons
exports.update = (req, res) => {
  const id = req.params.customerId;
  Persons.update( { firstname: req.body.firstname, lastname: req.body.lastname, age: req.body.age }, 
           { where: {id: req.params.customerId} }
           ).then(() => {
           res.status(200).send("updated successfully a customer with id = " + id);
           });
};
 
// Delete a Persons by Id
exports.delete = (req, res) => {
  const id = req.params.customerId;
  Persons.destroy({
    where: { id: id }
  }).then(() => {
    res.status(200).send('deleted successfully a customer with id = ' + id);
  });
};