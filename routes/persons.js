const express = require('express');
const router = express.Router();
const persons = require('../controllers/persons.controller');
const auth = require('../middleware/auth');

//router.get('/test', persons_controller.test);

// Create a new Customer
router.post('/api/persons', persons.create);

// Retrieve all Customer
router.get('/api/persons', auth, persons.findAll);

// Retrieve a single Customer by Id
router.get('/api/persons/:customerId', persons.findById);

// Update a Customer with Id
router.put('/api/persons/:customerId', persons.update);

// Delete a Customer with Id
router.delete('/api/persons/:customerId', persons.delete);

module.exports = router;