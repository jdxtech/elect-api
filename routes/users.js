const express = require('express');
const router = express.Router();
const users = require('../controllers/users.controller');
const auth = require('../middleware/auth');

router.post('/', users.createUser);

router.get('/me', auth, users.getCurrentUser);

module.exports = router;